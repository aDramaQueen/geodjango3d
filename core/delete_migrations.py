#!/usr/bin/env python3
import os
from glob import glob
from pathlib import Path
from typing import List, Optional

__MIGRATIONS_DIRECTORY_NAME__ = 'migrations'

__IGNORE_FILES__ = {'__init__.py', '__pycache__'}
__IGNORE_DIRECTORIES__ = {'venv'}
__PART_MIGRATIONS_TO_EXCLUDE__ = {'/django/db/migrations'}


def delete_migrations(root: Path, additional_ignore_directories: Optional[List[str]] = None, additional_ignore_files: Optional[List[str]] = None):
    if additional_ignore_directories:
        ignore_directories = __IGNORE_DIRECTORIES__.update(additional_ignore_directories)
    else:
        ignore_directories = __IGNORE_DIRECTORIES__

    if additional_ignore_files:
        ignore_files = __IGNORE_FILES__.update(additional_ignore_files)
    else:
        ignore_files = __IGNORE_FILES__

    deleted_at_least_one = False
    unfiltered_migration_directories: List[str] = glob(
        root.absolute().__str__() + '/**/' + __MIGRATIONS_DIRECTORY_NAME__, recursive=True)

    unwanted_directories = set()
    for directory in unfiltered_migration_directories:
        for unwanted in ignore_directories:
            if directory.__contains__(unwanted):
                unwanted_directories.add(directory)
    migration_directories = set(unfiltered_migration_directories) - unwanted_directories

    print('')
    for migration_dir in migration_directories:
        if not any([directory_part in migration_dir for directory_part in __PART_MIGRATIONS_TO_EXCLUDE__]):
            for file in os.listdir(migration_dir):
                file_path = Path(migration_dir, file).resolve()
                if file not in ignore_files:
                    deleted_at_least_one = True
                    os.remove(file_path)
                    print(f'Removed file: {file_path}')
    if not deleted_at_least_one:
        print(f'Did not found any migration files to delete')


if __name__ == '__main__':
    root = Path(__file__).resolve().parent.parent
    delete_migrations(root)
