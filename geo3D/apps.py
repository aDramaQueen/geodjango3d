from django.apps import AppConfig


class Geo3DConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'geo3D'
