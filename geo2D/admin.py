# ACHTUNG: admin kommt nicht von Django.contrib, sonder von Django.contrib.gis !!!
from django.contrib.gis import admin

from geo2D.models import WorldBorder

admin.site.register(WorldBorder, admin.GeoModelAdmin)
