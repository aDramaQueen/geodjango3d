from django.apps import AppConfig


class Geo2DConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'geo2D'
