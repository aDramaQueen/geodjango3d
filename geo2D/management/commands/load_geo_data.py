from logging import getLogger

from django.core.management.base import BaseCommand

from geo2D.load import run


class Command(BaseCommand):
    LOGGER = getLogger(__name__)
    help = f'Load Spatial Geo Data from file'

    def add_arguments(self, parser):
        parser.add_argument(
            '--verbose',
            action='store_false',
            help='Show debug infos during loading (DEFAULT: True)',
        )

    def handle(self, *args, **options):
        try:
            verbose: bool = options.get('verbose')
            run(verbose)

        except Exception as exc:
            raise exc
